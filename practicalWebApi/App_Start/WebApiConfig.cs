﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Tracing;

namespace practicalWebApi
{
	public static class WebApiConfig
	{
		public static void Register(HttpConfiguration config)
		{
			// Web API configuration and services

			// Web API routes
			//	config.MapHttpAttributeRoutes();

		//	config.EnableSystemDiagnosticsTracing();

		//	config.Services.Replace(typeof(ITraceWriter), new WebApiTracer());

			config.Services.Replace(
				typeof(System.Web.Http.Tracing.ITraceWriter),
				new EntryExitTracer());


			config.Routes.MapHttpRoute(
				name: "DefaultApi",
				routeTemplate: "api/{controller}/{id}",
				defaults: new { id = RouteParameter.Optional }
			);
		}
	}
}
