﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.NetworkInformation;
using System.Reflection;
using System.Web.Http.Tracing;
using Microsoft.Data.OData;

namespace practicalWebApi
{
	public class EntryExitTracer : ITraceWriter
	{
		public void Trace(HttpRequestMessage request, string category, TraceLevel level, Action<TraceRecord> traceAction)
		{
			if (level != TraceLevel.Off)
			{
				TraceRecord rec = new TraceRecord(request, category, level);
				traceAction(rec);


				RingBufferLog.Instance.Enqueue(rec);

			}
		}
	}

	public class RingBufferLog
	{
		private const int BUFFER_SIZE = 1000;

		private TraceRecord[] buffer;
		int pointer = 0;
		private readonly object myPrecious = new object();

		private static RingBufferLog instance = new RingBufferLog();

		private RingBufferLog()
		{
			buffer = new TraceRecord[BUFFER_SIZE];
			ResetPointer();
		}

		public IList<TraceRecord> DequeueAll()
		{
			lock (myPrecious)
			{
				ResetPointer();

				var bufferCopy = new List<TraceRecord>(buffer.Where(t => t != null));

				for (int index = 0; index < BUFFER_SIZE; index++)
				{
					buffer[index] = null;
				}

				return bufferCopy;
			}
		}

		public IList<TraceRecord> PeekAll()
		{
			lock (myPrecious)
			{
				var bufferCopy = new List<TraceRecord>(buffer.Where(t => t != null));
				return bufferCopy;
			}
		}

		private void ResetPointer()
		{
			pointer = BUFFER_SIZE -1;

		}

		private void MovePointer()
		{
			pointer = (pointer + 1) % BUFFER_SIZE;
		}

		public void Enqueue(TraceRecord item)
		{
			lock (myPrecious)
			{
				MovePointer();
				buffer[pointer] = item;
			}
		}

		public static RingBufferLog Instance
		{
			get { return instance; }
		}

	}
}